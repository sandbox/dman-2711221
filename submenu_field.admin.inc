<?php

/**
 * @file
 * UI settings for enabling these fields.
 */

/**
 * Form to set which content types get the fields added to them.
 *
 * Elaborate form just to fit the entire arcitecture of the fieldable
 * entities into one page.
 * Could probably be improved by abstracting into something automatic and
 * recursive - but in reality it would take even longer and get messier.
 *
 * Makes extensive advantage of system_form and #tree.
 *
 * Impliments hook_form().
 */
function submenu_field_settings_form($form, &$form_state) {

  // Build a huge form listing all fields/entities/bundles/view_modes.
  $defaults = variable_get('submenu_field_defaults', submenu_field_defaults());
  $form['submenu_field_defaults'] = array(
    '#type' => 'fieldset',
    '#title' => t('Field instances'),
    '#tree' => TRUE,
  );
  $fields = submenu_field_ids();

  // ENTITY TYPES.
  $entity_types = entity_get_info();
  foreach ($entity_types as $entity_type => $entity_info) {
    // Collapse all sections with no data in.
    $entity_open = FALSE;
    if (!$entity_info['fieldable']) {
      continue;
    }
    $form['submenu_field_defaults'][$entity_type] = array(
      '#type' => 'fieldset',
      '#title' => $entity_info['label'],
      '#tree' => TRUE,
      '#collapsible' => TRUE,
    );
    $view_modes = $entity_info['view modes'];

    // BUNDLES.
    foreach ($entity_info['bundles'] as $bundle => $bundle_info) {
      $bundle_open = FALSE;
      $form['submenu_field_defaults'][$entity_type][$bundle] = array(
        '#type' => 'fieldset',
        '#title' => $entity_info['label'] . " : " . $bundle_info['label'],
        '#tree' => TRUE,
        '#collapsible' => TRUE,
      );
      // This wrapper is added for consistent array structure.
      $form['submenu_field_defaults'][$entity_type][$bundle]['display'] = array(
        '#type' => 'container',
      );

      // Just list the view modes that are enabled for this bundle.
      $active_view_modes = array('default');
      $bundle_settings = field_bundle_settings($entity_type, $bundle);
      foreach ($bundle_settings['view_modes'] as $view_mode => $info) {
        if ($info['custom_settings']) {
          if (!isset($view_modes[$view_mode])) {
            // Dunno how, but sometimes the system has invalid modes listed.
            continue;
          }
          $active_view_modes[] = $view_mode;
        }
      }
      // VIEW MODES.
      foreach ($active_view_modes as $view_mode) {
        $view_mode_open = FALSE;
        $form['submenu_field_defaults'][$entity_type][$bundle]['display'][$view_mode] = array(
          '#type' => 'fieldset',
          '#title' => $bundle_info['label'] . ' : ' . (isset($view_modes[$view_mode]['label']) ? $view_modes[$view_mode]['label'] : $view_mode),
          '#collapsible' => TRUE,
          '#tree' => TRUE,
        );

        // FIELDS.
        foreach ($fields as $field_id) {
          $field_open = FALSE;
          $weight = isset($defaults[$entity_type][$bundle]['display'][$view_mode][$field_id]['weight']) ? $defaults[$entity_type][$bundle]['display'][$view_mode][$field_id]['weight'] : '';
          if ($weight != '') {
            $field_open = TRUE;
          }
          $form['submenu_field_defaults'][$entity_type][$bundle]['display'][$view_mode][$field_id] = array(
            '#type' => 'fieldset',
            '#title' => $field_id,
            '#tree' => TRUE,
            '#collapsible' => TRUE,
            '#collapsed' => !$field_open,
          );
          $form['submenu_field_defaults'][$entity_type][$bundle]['display'][$view_mode][$field_id]['weight'] = array(
            '#type' => 'textfield',
            '#size' => 2,
            '#title' => t('weight'),
            '#default_value' => $weight,
          );
          $view_mode_open |= $field_open;
        }
        $form['submenu_field_defaults'][$entity_type][$bundle]['display'][$view_mode]['#collapsed'] = !$view_mode_open;
        $bundle_open |= $view_mode_open;
      }
      $form['submenu_field_defaults'][$entity_type][$bundle]['#collapsed'] = !$bundle_open;
      $entity_open |= $bundle_open;
    }
    $form['submenu_field_defaults'][$entity_type]['#collapsed'] = !$entity_open;
  }
  $form['#submit'] = array('submenu_field_settings_form_submit');
  return system_settings_form($form);
}

/**
 * When saving the settings - discard all redundancies.
 *
 * Impliments hook_form_validate().
 */
function submenu_field_settings_form_validate($form, &$form_state) {
  $settings = $form_state['values']['submenu_field_defaults'];
  $clean_settings = submenu_field_array_filter_recursive($settings);
  form_set_value($form['submenu_field_defaults'], $clean_settings, $form_state);
}

/**
 * Remove all empty values from a deep array.
 *
 * Utility function.
 *
 * @param array $input
 *   Array to be filtered.
 *
 * @return array
 *   Clean array with no empty or null values.
 */
function submenu_field_array_filter_recursive($input) {
  foreach ($input as &$value) {
    if (is_array($value)) {
      $value = submenu_field_array_filter_recursive($value);
    }
  }
  return array_filter($input);
}

/**
 * When saving the settings - reset the fields instances to apply changes.
 *
 * Impliments hook_form_submit().
 */
function submenu_field_settings_form_submit($form, &$form_state) {
  // Variable_set has not run yet, so grab the value directly.
  $defaults = $form_state['values']['submenu_field_defaults'];
  submenu_field_reset($defaults);
  drupal_set_message(t('Updated field instances on the entity types.'));
}
