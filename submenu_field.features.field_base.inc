<?php
/**
 * @file
 * submenu_field.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function submenu_field_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'submenu_children_teasers'.
  $field_bases['submenu_children_teasers'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'submenu_children_teasers',
    'indexes' => array(),
    'locked' => 0,
    'module' => 'viewfield',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'viewfield',
  );

  // Exported field_base: 'submenu_children_list'.
  $field_bases['submenu_children_list'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'submenu_children_list',
    'indexes' => array(),
    'locked' => 0,
    'module' => 'viewfield',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'viewfield',
  );

  return $field_bases;
}
