<?php
/**
 * @file
 * submenu_views.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 *
 * Provides the initial views used by submenufield to list menu children of
 * the given context.
 */
function submenu_field_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'submenu_views';
  $view->description = 'Views of child items of given menu items';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Submenu views';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'node';
  $handler->display->display_options['row_options']['links'] = FALSE;
  /* Relationship: Menu: Parent menu link id */
  $handler->display->display_options['relationships']['plid']['id'] = 'plid';
  $handler->display->display_options['relationships']['plid']['table'] = 'menu_links';
  $handler->display->display_options['relationships']['plid']['field'] = 'plid';
  $handler->display->display_options['relationships']['plid']['required'] = TRUE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Sort criterion: Menu: Weight */
  $handler->display->display_options['sorts']['weight']['id'] = 'weight';
  $handler->display->display_options['sorts']['weight']['table'] = 'menu_links';
  $handler->display->display_options['sorts']['weight']['field'] = 'weight';
  /* Contextual filter: Menu: Parent menu link id */
  $handler->display->display_options['arguments']['plid']['id'] = 'plid';
  $handler->display->display_options['arguments']['plid']['table'] = 'menu_links';
  $handler->display->display_options['arguments']['plid']['field'] = 'plid';
  $handler->display->display_options['arguments']['plid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['plid']['default_argument_type'] = 'menu_item';
  $handler->display->display_options['arguments']['plid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['plid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['plid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['plid']['specify_validation'] = TRUE;
  /* Filter criterion: Content: Published or admin */
  $handler->display->display_options['filters']['status_extra']['id'] = 'status_extra';
  $handler->display->display_options['filters']['status_extra']['table'] = 'node';
  $handler->display->display_options['filters']['status_extra']['field'] = 'status_extra';
  $handler->display->display_options['filters']['status_extra']['group'] = 1;

  /* Display: Submenu Children Teaser Block */
  $handler = $view->new_display('block', 'Submenu Children Teaser Block', 'submenu_children_teasers');
  $handler->display->display_options['display_comment'] = 'This block can be placed as usual via block positioning, in which case it will show the menu children of the current page.
Or it can be added to node content using viewfield - in which case it should be passed the nodes menu link ID as a argument from tokens. Set Arguments = [node:menu-link:mlid]';
  $handler->display->display_options['display_description'] = 'The menu children of the given context, presented as a list of teasers.';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Menu: Parent menu link id */
  $handler->display->display_options['arguments']['plid']['id'] = 'plid';
  $handler->display->display_options['arguments']['plid']['table'] = 'menu_links';
  $handler->display->display_options['arguments']['plid']['field'] = 'plid';
  $handler->display->display_options['arguments']['plid']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['plid']['default_argument_type'] = 'menu_item';
  $handler->display->display_options['arguments']['plid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['plid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['plid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['plid']['specify_validation'] = TRUE;

  /* Display: Submenu Children List Block */
  $handler = $view->new_display('block', 'Submenu Children List Block', 'submenu_children_list');
  $handler->display->display_options['display_description'] = 'The menu children of the given context, presented as a menu-like list of links.';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['row_class'] = 'collapsed';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['class'] = 'menu';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Menu: Link title */
  $handler->display->display_options['fields']['link_title']['id'] = 'link_title';
  $handler->display->display_options['fields']['link_title']['table'] = 'menu_links';
  $handler->display->display_options['fields']['link_title']['field'] = 'link_title';
  $handler->display->display_options['fields']['link_title']['label'] = '';
  $handler->display->display_options['fields']['link_title']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['link_title']['alter']['alt'] = '[title]';
  $handler->display->display_options['fields']['link_title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['link_title']['link_to_item'] = 1;
  $export['submenu_views'] = $view;

  return $export;
}
