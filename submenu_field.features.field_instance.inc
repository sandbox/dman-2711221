<?php
/**
 * @file
 * submenu_field.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 *
 * Do not call this directly - it's a reference template for the format of
 * expected field_instance info arrays.
 */
function submenu_field_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-page-submenu_children'.
  $field_instances['node-page-submenu_children_teasers'] = array(
    'bundle' => 'page',
    'default_value' => array(
      0 => array(
        'vname' => 'submenu_views|submenu_children_teasers',
        'vargs' => '[node:menu-link:mlid]',
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'viewfield',
        'settings' => array(),
        'type' => 'viewfield_default',
        'weight' => 20,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 20,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'submenu_children_teasers',
    'label' => 'Submenu Children Teasers',
    'required' => 0,
    'settings' => array(
      'allowed_views' => array(
        'submenu_views' => 'submenu_views',
      ),
      'force_default' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'viewfield',
      'settings' => array(),
      'type' => 'viewfield_select',
      'weight' => 20,
    ),
  );

  return $field_instances;
}
