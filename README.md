# Submenu Field

A pseudo field and a view block
 that displays the menu children of a given context.

This provides rich tree-like navigation for heirarchically structured sites.

![Submenu fields in both full+teaser and teaser+menu modes](submenu_fields-smaller.png)

## Usage

The new fields can be added directly to page layouts through field UI,
 or the blocks can be positioned in a region.

There is also an optional UI for adding these fields from the top down,
 found at admin/config/user-interface/submenu_field .
 The UI is for convenience only, and is not a replacement for real field
 UI display management.

If you wish to use different types of view on different view modes:
 Like showing child teasers on full-page,
 and just showing child titles as part of a teaser,
 it's neccessary to use two different fields.
 This module provides both submenu_children_teasers and submenu_children_list
 for these different cases. You should choose and display them separately
 in the respective view_modes.

### Enable caching

Due to the relatively static nature of menus, it is highly recommended
you use views caching with this module,
especially when attaching submenus to teasers.

### Automation

As this utility is suited to assisting automated configurations
 (such as install profiles) it's possible to influence its behaviour from
 outside. There is a system variable `submenu_field_defaults` which can
 be packed to include settings enumerating all content types and view modes
 this new field should be attached to.

 If you set this variable to say that you want 'page' 'teaser' to display the
 submenu titles below the content, then enable this module, that will happen.
 You can also call `submenu_field_reset()` at any time to apply that change.

 See `submenu_field_reset()` and `submenu_field_defaults()` for details.

## Extending

This module leverages
 [viewfield module](https://www.drupal.org/project/viewfield),
 and that's the tool that provides most of the visible field configurations.
 You can check the docs there about making changes.
The provided field definition sets the default arguments to always
 be the same (and the view to be shown) on all view modes this is enabled for.

It's possible to give the editor per-node control of which view display
 (if any) is shown by adjusting the field settings to **not**
 *"Always use default value"* in the settings:

    /admin/structure/types/manage/{*}/fields/field_menu_children

But you probably don't want to do that.

## Development details

The functionality provided here *can* be constructed entirely manually
 by using the supporting contrib modules and configuring by hand.

This module mainly exists to bundle the settings and ease initial setup.

It is expected that you may wish to clone, extend or modify the provided
 view to suit your needs.

### Supporting modules

[menu_node_views](https://www.drupal.org/project/menu_node_views)
and
[menu_node](https://www.drupal.org/project/menu_node)
and
[viewfield](https://www.drupal.org/project/viewfield)
are required.

Alternatives to [viewfield](https://www.drupal.org/project/viewfield) include:

* [EVA](https://www.drupal.org/project/eva) Powerful, but not used in submenu_field as it places all the management in the view itself, and not enough control at the per-bundle, per-view_mode settings where I need it.
* [views_field_formatter](https://www.drupal.org/project/views_field_formatter) Could have been good, as the settings are somewhat in the formatter where I wanted it. Lacks the per-node config option... though that may be an advantage. Could have been a lightweight alternative, but the view config process is odd, and the way it inserts itself into existing fields to override them (instead of providing a pseudo field via hook_field_extra_fields()) is creepy.
I can see what it's trying to do with passing field data in as arguments to a view that then does more things, but that's not appropriate for submenu_field.

### Wishlist

Ideally for my needs, the view configs (choice of view display and args) could
 be presented in field_formatter_settings (display), not field_settings (data).
But that would be a whole new module, and I think viewfield suits the purpose
 enough for now.

### Features - not

We are NOT using features.module.
But we use some of their file and data conventions.

The 'features' files distributed with this module were generated in the
 usual format provided by Features export.
 However we don't need to use Features to enable them or anything.
 Instead, these config arrays are template loaded on demand and used to seed the
 field CRUD API functions directly.

This enables us to soft-toggle the field instances on as needed, while still
 retaining some code familiarity within the features.inc files for future
 reference or updates.


